# Imageboard thread scraper

Simple python script to scrap all the files from a thread. Uses only standard library. Should work with most imageboards.

usage: **threadScraper.py** *url* *dest_directory*

If no destination provided, writes to the current directory

Scraps:
* .jpg
* .png
* .gif
* .webm
* .mp4
* .pdf

If you want to include any other format, just edit line 20 and add a proper file extention.