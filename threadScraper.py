#!/usr/bin/env python
#
# created by: __karras
# https://gitlab.com/__karras/
#

from html.parser import HTMLParser
import urllib.request
import re
import os
import sys

class MyHTMLParser(HTMLParser):
    files = []
    def handle_starttag(self, tag, attrs):
        if tag == 'a':
            for atr in attrs:
                if atr[0] == 'href':
                    if bool(re.match(r".*\.((jpg)|(png)|(gif)|(webm)|(mp4)|(pdf))$",atr[1]) and atr[1] not in self.files):
                        self.files.append(atr[1])

def getPage():
    file = open("./tmp_page.html", "r")
    input = ""
    for line in file:
        input = input + line
    file.close()
    os.remove("./tmp_page.html")
    return input

def main():
    dir=""
    if(len(sys.argv) < 2):
        print("ERROR! Provide parameters")
        print("usage: threadScraper.py url dest_directory")
        print("       if no destination provided, writes to the current directory")
        return -1
    elif(len(sys.argv) == 2):
        dir = "./"
    else:
        if(not os.path.exists(sys.argv[2])):
            os.mkdir(sys.argv[2])
        dir=sys.argv[2]


    print("Starting...")
    parser = MyHTMLParser()
    url = sys.argv[1]
    
    opener=urllib.request.build_opener()
    opener.addheaders=[('User-Agent','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1941.0 Safari/537.36')]
    urllib.request.install_opener(opener)
    urllib.request.urlretrieve(url,"./tmp_page.html")
    input = getPage()

    parser.feed(input)
    for entry in parser.files:
        filename=(entry[str(entry).rfind('/'):])
        print ("Downloading "+filename+"... ")
        entry = 'https:' + entry
        urllib.request.urlretrieve(entry,dir+filename)

    print("Finished!")

if __name__ == "__main__":
    main();
